<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_kamar extends CI_Model{
    var $table = 'kamar';

    function __construct() {
        parent::__construct();
    }

    //get data kamar
    function getData() {
        $hasil = $this->db->query('SELECT kd_kamar, no_kamar, jenis_kamar.deskripsi as jenis_kamar, status, kamar.kd_jenis_kamar
                            FROM kamar INNER JOIN jenis_kamar ON kamar.kd_jenis_kamar = jenis_kamar.kd_jenis_kamar 
                            ORDER BY jenis_kamar')->result();
        return $hasil;
    }

    //get data kamar yang status kamarnya kosong
    function getDataKosong() {
        $hasil = $this->db->query("SELECT kd_kamar, no_kamar, jenis_kamar.deskripsi as jenis_kamar, status, kamar.kd_jenis_kamar
                            FROM kamar INNER JOIN jenis_kamar ON kamar.kd_jenis_kamar = jenis_kamar.kd_jenis_kamar 
                            WHERE status = 'e'")->result();
        return $hasil;
    }

    //get all data yang id = $id
    function getDataWithId($id) {
        // $this->db->where('id_transaksi', $id); 
        $hasil = $this->db->query('SELECT kd_kamar, no_kamar, jenis_kamar.deskripsi as jenis_kamar, kamar.kd_jenis_kamar, status
                            FROM kamar INNER JOIN jenis_kamar ON kamar.kd_jenis_kamar = jenis_kamar.kd_jenis_kamar 
                            WHERE kd_kamar = '.$id.'')->row();
        return $hasil;
    }

    function getJmlData() {
        $hasil = $this->db->get($this->table)->num_rows();
        return $hasil;
    }

    //sum jumlah kamar berdasarkan jenis untuk keperluan dashboard
    function sumKamarByJenis() {
        $hasil = $this->db->query('SELECT COUNT(kd_kamar) as jumlah, jenis_kamar.deskripsi as jenis_kamar
                            FROM kamar INNER JOIN jenis_kamar ON kamar.kd_jenis_kamar = jenis_kamar.kd_jenis_kamar 
                            GROUP BY jenis_kamar')->result();
        return $hasil;
    }
}

