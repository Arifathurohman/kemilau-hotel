<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_action extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    

    //general insert query
    public function insert($table, $data){
        $qry = $this->db->insert($table, $data);
        
        if($qry){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    //general update function
    public function update($table, $param, $where, $data){
        $this->db->where($param, $where);
        $qry = $this->db->update($table, $data);
        
        if($qry){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    //general delete function
    function delete($param1, $param2, $param3) {
        $this->db->where($param2, $param3);

        $qry = $this->db->delete($param1);

        return $qry;
    }
    
   
}