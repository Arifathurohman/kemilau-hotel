<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_reservasi extends CI_Model{
    //nama table di database
    var $table = 'reservasi';

    function __construct() {
        parent::__construct();
    }

    //get data reservasi
    function getData() {
        $hasil = $this->db->query('SELECT kd_rsv, nama, no_hp, reservasi.kd_kamar as kd_kamar, jenis_kamar.deskripsi as jenis_kamar, 
                            kamar.no_kamar as no_kamar, jenis_kamar.harga as harga, status, checked_out, tgl_in, tgl_out, total_bayar
                            FROM reservasi INNER JOIN kamar ON reservasi.kd_kamar = kamar.kd_kamar
                            INNER JOIN  jenis_kamar ON kamar.kd_jenis_kamar = jenis_kamar.kd_jenis_kamar')->result();
        return $hasil;
    }

    //get data reservasi dengan parameter id
    function getDataWithId($id) {
        // $this->db->where('id_transaksi', $id); 
        $hasil = $this->db->query('SELECT kd_rsv, nama, no_hp, reservasi.kd_kamar as kd_kamar, jenis_kamar.deskripsi as jenis_kamar, 
                            kamar.no_kamar as no_kamar, jenis_kamar.harga as harga, checked_out, tgl_in, tgl_out, total_bayar
                            FROM reservasi INNER JOIN kamar ON reservasi.kd_kamar = kamar.kd_kamar
                            INNER JOIN  jenis_kamar ON kamar.kd_jenis_kamar = jenis_kamar.kd_jenis_kamar 
                            WHERE kd_rsv = '.$id.'')->result();
        return $hasil;
    }

    //get data transaksi harian
    function sumTransaksiHarian(){
        $hasil = $this->db->query("SELECT tgl_in, sum(total_bayar) as grand_total
                        FROM reservasi 
                        GROUP BY tgl_in")->result();
        return $hasil;
    }

    function getJmlData() {
        $hasil = $this->db->get($this->table)->num_rows();
        return $hasil;
    }

    //mengambil row data yanag hari ini checkout untuk keperluan notifikasi
    function getCheckoutNow(){
        $this->db->select('kd_rsv, nama, tgl_out');
        $this->db->where('tgl_out', date('Y-m-d'));
        $this->db->from('reservasi');

        $hasil = $this->db->get()->result();

        return $hasil;
    }

}

