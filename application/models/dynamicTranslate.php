<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DynamicTranslate extends CI_Model {

    function translate($table, $kode, $param, $deskripsi) {

        /* ===========================================================================
         * 	Ini udah dibuat dinamis,, nanti pake yg ini aja.. :)
         * =========================================================================== */

        if ($param != NULL) {
            $qry = $this->db->get_where($table, array($kode => $param));
            $name = $qry->row_array();
            return $name[$deskripsi];
        } else {
            return "-";
        }
    }

}
