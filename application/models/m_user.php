<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_user extends CI_Model{
    var $table = 'users';
   
    function __construct() {
        parent::__construct();
    }

    function login($user, $pass) {

        //ambil data user dan pass masukan ke array
        $loginArray = array(
            'username' => $user,
            'password' => $pass
        );

        //active record buat cek di db data yang username dan passswordnya
        //sama
        $this->db->where($loginArray);
        $login = $this->db->get('users');

        //kalau ada return data dalam bentuk row array
        if ($login->num_rows() == 1) {
            return $login->row_array();
        } else {
            //kalau gak ada return false
            return FALSE;
        }
    }

    //get all users row
    function getData() {
        $hasil = $this->db->get($this->table)->result();
        return $hasil;
    }

    //get count
    function getJmlData() {
        $hasil = $this->db->get($this->table)->num_rows();
        return $hasil;
    }

    //get row where id = $id
    function getDataById($id) {
        $this->db->where('kd_user', $id);
        $hasil = $this->db->get($this->table)->row();
        return $hasil;
    }
    
}