<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_jenisKamar extends CI_Model{
    var $table = 'jenis_kamar';

    function __construct() {
        parent::__construct();
    }

    //get all data
    function getData() {
        $hasil = $this->db->get($this->table)->result();
        return $hasil;
    }

    //get count data
    function getJmlData() {
        $hasil = $this->db->get($this->table)->num_rows();
        return $hasil;
    }

    //get data dengan id = $id
    function getDataById($id) {
        $this->db->where('kd_jenis_kamar', $id);
        $hasil = $this->db->get($this->table)->row();
        return $hasil;
    }

    //get kolom harga berdasarkan id = $id
    function getHargaKamar($id) {
        $this->db->select('harga');
        $this->db->where('kd_jenis_kamar', $id);
        $hasil = $this->db->get($this->table)->row();
        return $hasil;
    }
}

