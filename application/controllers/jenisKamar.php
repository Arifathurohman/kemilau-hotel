<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class JenisKamar extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('M_jenisKamar', 'M_action','DynamicTranslate'));
        
        //is login?
        if ($this->session->userdata('logged_in') == FALSE) {
            redirect();
        }
    }

    //menampilkan halaman index jenis kamar
    public function index() {
        $data['title'] = sysadmin_url . 'Jenis Kamar';
        $data['jenisKamar'] = $this->M_jenisKamar->getData();

        $this->load->view('jenisKamar', $data);
    }

    //menampilkan halaman input data
    public function input(){
        $data['title'] = sysadmin_url . 'Input Jenis Kamar';

        $this->load->view('form/jenisKamar', $data);
    }

    //proses submit data
    public function simpan(){
        $data = array(
            'deskripsi' => $this->input->post('deskripsi'),
            'harga' => $this->input->post('harga')
        );

        $query = $this->M_action->insert('jenis_kamar', $data);

        if($query){
            $this->session->set_flashdata('success', 'data berhasil ditambah');
            redirect('jenisKamar');
        }else{
            $this->session->set_flashdata('gagal', 'data gagal ditambah');
            redirect('jenisKamar');
        }
    }

    //menampilkan halaman edit data
    public function edit($id){
        $data['title'] = sysadmin_url . 'Edit Jenis Kamar';
        $data['jenisKamar'] =  $this->M_jenisKamar->getDataById($id);

        $this->load->view('form/jenisKamar_edit', $data);
    }

    //proses update data
    public function update(){
        $kd_jenis_kamar = $this->input->post('kd_jenis_kamar');
        $data = array(

            'deskripsi' => $this->input->post('deskripsi'),
            'harga' => $this->input->post('harga')
        );

        $query = $this->M_action->update('jenis_kamar', 'kd_jenis_kamar', $kd_jenis_kamar, $data);

        if($query){
            $this->session->set_flashdata('update', 'data berhasil diupdate');
            redirect('jenisKamar');
        }else{
            $this->session->set_flashdata('gagal', 'data gagal ditambah');
            redirect('jenisKamar');
        }

    }

    //proses hapus data
    public function hapus($id){
        $query = $this->M_action->delete('jenis_kamar', 'kd_jenis_kamar', $id);

        if($query){
            $this->session->set_flashdata('delete', 'data berhasil dihapus');
            redirect('jenisKamar');
        }else{
            $this->session->set_flashdata('gagal', 'data gagal ditambah');
            redirect('jenisKamar');
        }
    }

}