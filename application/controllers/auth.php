<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_user');
    }

    public function login() {

        //cek apakah input dari form kosong atau tidak
        if ($this->input->post('username') == NULL && $this->input->post('password') == NULL) {
            redirect();
        }

        //cek apakah di db ada user pas seperti yang diinputkan atau tidak
        $login = $this->M_user->login($this->input->post('username'), $this->input->post('password'));

        
        if ($login <> FALSE) {
            //pembuatan seassion
            $sess_array = array();
            $sess_array = array(
                'kd_user' => $login['kd_user'],
                'username' => $login['username'],
                'user_lvl' => $login['user_lvl'],
                'logged_in' => TRUE
            );
            $this->session->set_userdata($sess_array);
            if ($this->session->userdata('user_lvl') == 'admin') {
                 //kalau user = admin
                redirect('reservasi');
            } else {
                //kalau user = owner
                redirect('dashboard','refresh');
            }
        } else {
            $this->session->set_flashdata('error', 'kagak bisa login');
            redirect();
        }
    }

    public function logout() {

        $this->session->sess_destroy();

        redirect();
    }

}
