<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('M_user', 'M_action'));
        
        //is login?
        if ($this->session->userdata('logged_in') == FALSE) {
            redirect();
        }
    }

    //menampilkan data login
    public function index() {
        $data['title'] = sysadmin_url . 'Users';
        $data['users'] = $this->M_user->getData();

        $this->load->view('users', $data);
    }

    //menampilkan halaman input data
    public function input(){
        $data['title'] = sysadmin_url . 'Input Users';

        $this->load->view('form/users', $data);
    }

    //proses simpan data
    public function simpan(){
        
        $data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'user_lvl' => $this->input->post('user_lvl')
        );

        $query = $this->M_action->insert('users', $data);

        if($query){
            $this->session->set_flashdata('success', 'data berhasil ditambah');
            redirect('users');
        }else{
            $this->session->set_flashdata('gagal', 'data berhasil ditambah');
            redirect('users');
        }
    }

    //menampilkan halaman edit data
    public function edit($id){
        $data['title'] = sysadmin_url . 'Edit Users';
        $data['users'] =  $this->M_user->getDataById($id);

        $this->load->view('form/users_edit', $data);
    }

    //proses update data
    public function update(){
        $kd_user = $this->input->post('kd_user');
        $data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),
            'user_lvl' => $this->input->post('user_lvl')
            );

        $query = $this->M_action->update('users', 'kd_user', $kd_user, $data);

        if($query){
            $this->session->set_flashdata('update', 'data berhasil diupdate');
            redirect('users');
        }else{
            $this->session->set_flashdata('gagal', 'data berhasil ditambah');
            redirect('users');
        }

    }

    //proses hapus data
    public function hapus($id){
        $query = $this->M_action->delete('users', 'kd_user', $id);

        if($query){
            $this->session->set_flashdata('delete', 'data berhasil dihapus');
            redirect('users');
        }else{
            $this->session->set_flashdata('gagal', 'data berhasil ditambah');
            redirect('users');
        }
    }

}