<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Kamar extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('M_kamar', 'M_jenisKamar', 'M_action','DynamicTranslate'));
        
        //is login?
        if ($this->session->userdata('logged_in') == FALSE) {
            redirect();
        }
    }

    //menampilan halaman data kamar
    public function index() {
        $data['title'] = sysadmin_url . 'kamar';
        $data['kamar'] = $this->M_kamar->getData();

        $this->load->view('kamar', $data);
    }

    //menampilkan halaman input data
    public function input(){
        $data['title'] = sysadmin_url . 'Input  Kamar';
        $data['jenisKamar'] = $this->M_jenisKamar->getData();

        $this->load->view('form/kamar', $data);
    }

    //proses submit data
    public function simpan(){
        $data = array(
            'no_kamar' => $this->input->post('no_kamar'),
            'kd_jenis_kamar' => $this->input->post('kd_jenis_kamar'),
            'status' => $this->input->post('status')
        );

        $query = $this->M_action->insert('kamar', $data);

        if($query){
            $this->session->set_flashdata('success', 'data berhasil ditambah');
            redirect('kamar');
        }else{
            $this->session->set_flashdata('gagal', 'data gagal ditambah');
            redirect('kamar');
        }
    }

    //menampilkan halaman edit data
    public function edit($id){
        $data['kamar'] =  $this->M_kamar->getDataWithId($id);
        $data['jenisKamar'] = $this->M_jenisKamar->getData();

        $this->load->view('form/kamar_edit', $data);
    }

    //proses update data
    public function update(){
        $kd_kamar = $this->input->post('kd_kamar');
        $data = array(

            'no_kamar' => $this->input->post('no_kamar'),
            'kd_jenis_kamar' => $this->input->post('kd_jenis_kamar'),
            'status' => $this->input->post('status')
        );

        $query = $this->M_action->update('kamar', 'kd_kamar', $kd_kamar, $data);

        if($query){
            $this->session->set_flashdata('update', 'data berhasil diupdate');
            redirect('kamar');
        }else{
            $this->session->set_flashdata('gagal', 'data gagal ditambah');
            redirect('kamar');
        }

    }

    //proses hapus data
    public function hapus($id){
        $query = $this->M_action->delete('kamar', 'kd_kamar', $id);

        if($query){
            $this->session->set_flashdata('delete', 'data berhasil dihapus');
            redirect('kamar');
        }else{
            $this->session->set_flashdata('gagal', 'data gagal ditambah');
            redirect('kamar');
        }
    }

}