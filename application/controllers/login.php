<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Login extends CI_Controller{
    public function __construct() {
        parent::__construct();
    }
    
    //menampilkan halaman login
    public function index(){
        if($this->session->userdata('logged_in') == TRUE){
            redirect('dashboard');
        }
        
        $data['title'] = sysadmin_url . 'LOGIN';
        
        $this->load->view('login', $data);
    }
}