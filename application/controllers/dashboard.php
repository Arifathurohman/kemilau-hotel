<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('M_reservasi', 'M_kamar'));
        
        //is login?
        if ($this->session->userdata('logged_in') == FALSE) {
            redirect();
        }
    }

    public function index() {
        //grab data yang akan di kirim ke view
        $data['title'] = sysadmin_url . 'Dashboard';
        $data['totalTransaksiHarian'] = $this->M_reservasi->sumTransaksiHarian();
        $data['totalKamarByJenis'] = $this->M_kamar->sumKamarByJenis();

        //load view berikut dengan data;
        $this->load->view('dashboard', $data);
    }

}