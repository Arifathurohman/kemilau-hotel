<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Reservasi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('M_reservasi', 'M_kamar', 'M_jenisKamar', 'M_action','DynamicTranslate'));
        
        //is login?
        if ($this->session->userdata('logged_in') == FALSE) {
            redirect();
        }
    }

    //menampilkan halaman data reservasi
    public function index() {
        $data['title'] = sysadmin_url . 'Reservasi';
        $data['reservasi'] = $this->M_reservasi->getData();

        $this->load->view('reservasi', $data);
    }

    //menampilkan halaman form check in
    public function check_in(){
        $data['title'] = sysadmin_url . 'Input  Kamar';
        $data['jenisKamar'] = $this->M_jenisKamar->getData();
        $data['kamar'] = $this->M_kamar->getDataKosong();

        $this->load->view('form/check_in', $data);
    }

    //proses simpan data
    public function simpan(){
        //proses inisialissasi tgl in tgl out
        $tgl_in = date('Y-m-d', strtotime($this->input->post('tgl_in')));
        $lama = $this->input->post('lama');
        $date = new DateTime($tgl_in);
        $date->modify('+'.$lama.' day');
        $tgl_out = $date->format('Y-m-d');

        $kd_kamar = $this->input->post('kd_kamar');
        $harga = $this->M_jenisKamar->getHargaKamar($this->input->post('kd_jenis_kamar'));


        $total_bayar = $harga->harga * $lama;
        $data = array(
            'nama' => $this->input->post('nama'),
            'no_hp' => $this->input->post('no_hp'),
            'kd_kamar' => $kd_kamar,
            'tgl_in' => $tgl_in,
            'tgl_out' => $tgl_out,
            'total_bayar' =>$total_bayar,
            'checked_out' =>'no'
        );


        
        $query = $this->M_action->insert('reservasi', $data);
        //jika query memasukan data kamar berhasil, ubah status kamar yang terpakai menjadi 'f'
        if($query){

            $data = array(
            'status' => 'f'
            );

            $query = $this->M_action->update('kamar', 'kd_kamar', $kd_kamar, $data);
            if($query)
            {
               $this->session->set_flashdata('success', 'data berhasil ditambah');
            redirect('reservasi'); 
            }
            else
            {
                $this->session->set_flashdata('gagal', 'data gagal ditambah');
            redirect('reservasi');
            }
            
        }else{
            $this->session->set_flashdata('gagal', 'data gagal ditambah');
            redirect('reservasi');
        }
    }

    //proses checkout kamar
    public function check_out($id,$id_kamar)
    {
        $kd_rsv = $id;
        $kd_kamar = $id_kamar;

        $data = array(
        'status' => 'e'
        );
        //mengubah status kamar yang tadinya 'f' menjadi 'e'
        $query = $this->M_action->update('kamar', 'kd_kamar', $kd_kamar, $data);
        if($query){

            $data = array(
            'checked_out' => 'yes'
            );
            //mengubah data reservasi terpilih menjadi checked_out = yes
            $query = $this->M_action->update('reservasi', 'kd_rsv', $kd_rsv, $data);
            if($query)
            {
                //redirect ke halaman reservasi detail
                redirect('reservasi/detail/'.$kd_rsv); 
            }
            else
            {
                //redirect ke halaman data reservasi
                redirect('reservasi');
            }
            
        }else{
            $this->session->set_flashdata('gagal_checkout', 'data gagal ditambah');
            redirect('reservasi');
        }
    }

    //menampilkan detail transaksi dari row terpilih
    public function detail($id)
    {
        $data['title'] = sysadmin_url . 'Reservasi - Detail';
        $data['reservasi'] = $this->M_reservasi->getDataWithId($id);

        $this->load->view('reservasi_detail', $data);
    }

    //menampilkan halaman nota
    public function cetakNota($id)
    {

        $data['title'] = sysadmin_url . 'Reservasi - Detail';
        $data['reservasi'] = $this->M_reservasi->getDataWithId($id);

        $this->load->view('cetakNota', $data);
    }

}