<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $title; ?></title>

    <!--style here -->
    <?= $this->load->view('slice/style'); ?>

</head>

<body>

    <div id="wrapper">

        <!--  navigation bar start here -->
        <?= $this->load->view('slice/navigation'); ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Jenis Kamar</h1>
                        <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?= base_url(); ?>jenisKamar/simpan" method="POST">
                                        <div class="form-group">
                                            <label>Deskripsi</label>
                                            <input class="form-control" name="deskripsi" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Harga</label>
                                            <input type="number" class="form-control" name="harga" id="harga" required>
                                        </div>
                                        <button class="btn btn-default" type="submit">Submit Button</button>
                                        <button class="btn btn-default" type="reset">Reset Button</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Script here -->
    <?= $this->load->view('slice/script'); ?>
    <!-- Formatting currency -->
    <script src="<?= sysadmin_assets ?>bower_components/jquery-maskMoney/jquery.maskMoney.js" type="text/javascript"></script>
    <script>
        function removeMask(){
             $("#harga").maskMoney('unmasked')[0];

             return true;
        }

        $(function() {
            $("#harga").maskMoney({prefix:'Rp. ', allowZero: true, precision: 0, allowNegative: true, thousands:'.', decimal:'.', affixesStay: false});
        })
    </script>

</body>

</html>
