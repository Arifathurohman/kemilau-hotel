<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $title; ?></title>

    <!--style here -->
    <?= $this->load->view('slice/style'); ?>

</head>

<body>

    <div id="wrapper">

        <!--  navigation bar start here -->
        <?= $this->load->view('slice/navigation'); ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Kamar</h1>
                        <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?= base_url(); ?>kamar/update" method="POST">
                                    <input type="hidden" name="kd_kamar" value="<?= $kamar->kd_kamar; ?>">
                                        <div class="form-group">
                                            <label>Nomer Kamar</label>
                                            <input class="form-control" name="no_kamar" value="<?= $kamar->no_kamar; ?>" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Jenis Kamar</label>
                                            <select class="form-control" name="kd_jenis_kamar">
                                                <option value="<?= $kamar->kd_jenis_kamar; ?>"><?= $kamar->jenis_kamar; ?></option>
                                                <?php
                                                foreach ($jenisKamar as $row) {
                                            ?>
                                                <option value="<?= $row->kd_jenis_kamar; ?>"><?= $row->deskripsi; ?></option>
                                            <?php
                                                }
                                            ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Status</label>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" id="status1" value="e" <?php echo ($kamar->status == 'e') ? 'checked' : ''; ?>>Empty
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" id="status2" value="f" <?php echo ($kamar->status == 'f') ? 'checked' : ''; ?>>full
                                            </label>
                                        </div>
                                        <button class="btn btn-default" type="submit">Submit Button</button>
                                        <button class="btn btn-default" type="reset">Reset Button</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Script here -->
    <?= $this->load->view('slice/script'); ?>

</body>

</html>
