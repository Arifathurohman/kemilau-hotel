<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $title; ?></title>

    <!--style here -->
    <?= $this->load->view('slice/style'); ?>

</head>

<body>

    <div id="wrapper">

        <!--  navigation bar start here -->
        <?= $this->load->view('slice/navigation'); ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Users</h1>
                        <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?= base_url(); ?>users/simpan" method="POST">
                                        <div class="form-group">
                                            <label>username</label>
                                            <input class="form-control" name="username" required>
                                        </div>
                                        <div class="form-group">
                                            <label>password</label>
                                            <input class="form-control" name="password" type="password" required>
                                        </div>
                                        <div class="form-group">
                                            <label>User Level</label>
                                            <div class="radio">
                                                <label><input type="radio" value="owner" name="user_lvl" checked="checked">Owner</label>
                                            </div>
                                            <div class="radio">
                                              <label><input type="radio" value="admin" name="user_lvl">Admin</label>
                                            </div>
                                        </div>
                                        <button class="btn btn-default" type="submit">Submit Button</button>
                                        <button class="btn btn-default" type="reset">Reset Button</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Script here -->
    <?= $this->load->view('slice/script'); ?>

</body>

</html>
