<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $title; ?></title>

    <!--style here -->
    <?= $this->load->view('slice/style'); ?>

</head>

<body>

    <div id="wrapper">

        <!--  navigation bar start here -->
        <?= $this->load->view('slice/navigation'); ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Check In</h1>
                        <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?= base_url(); ?>reservasi/simpan" method="POST">
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input class="form-control" name="nama" required>
                                        </div>
                                        <div class="form-group">
                                            <label>No HP</label>
                                            <input class="form-control" name="no_hp" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Check In</label>
                                            <input class="form-control datepicker" name="tgl_in" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Lama Tinggal</label>
                                            <input class="form-control" name="lama" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Jenis Kamar</label>
                                            <select class="form-control" id="jenisKamar" name="kd_jenis_kamar">
                                                <option value="">--</option>
                                                <?php
                                                 foreach ($jenisKamar as $row) {
                                                        echo '<option value="'.$row->kd_jenis_kamar.'">'.$row->deskripsi.' (Rp.'.$row->harga.'/malam)</option>';
                                                    }   
                                                ?>  
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>No Kamar</label>
                                            <select class="form-control" id="kamar" name="kd_kamar">
                                               <option value="">--</option>
                                               <?php
                                                    foreach ($kamar as $row) {
                                                        

                                                        echo '<option value="'.$row->kd_kamar.'" class="'.$row->kd_jenis_kamar.'">'.$row->no_kamar.'</option>';
                                                    }   
                                                ?>  
                                            </select>
                                        </div>
                                        <input class="btn btn-default" type="submit" value="Simpan">
                                        <input class="btn btn-default" type="reset" value="reset">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Script here -->
    <?= $this->load->view('slice/script'); ?>
    <?= $this->load->view('slice/script_chained_select'); ?>
    
</body>

</html>
