<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $title; ?></title>

    <!--style here -->
    <?= $this->load->view('slice/style'); ?>

</head>

<body>

    <div id="wrapper">

        <!--  navigation bar start here -->
        <?= $this->load->view('slice/navigation'); ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Users</h1>
                        <?php if ($this->session->flashdata('gagal')) {
                            ?>
                            <div class="alert alert-warning alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                Data gagal di proses!
                            </div>
                            <?php
                        }
                        ?>
                        <?php if ($this->session->flashdata('success')) {
                            ?>
                            <div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                Data User berhasil disimpan!
                            </div>
                            <?php
                        }
                        ?>
                        <?php if ($this->session->flashdata('delete')) {
                            ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                Data User berhasil dihapus!
                            </div>
                            <?php
                        }
                        ?>
                        <?php if ($this->session->flashdata('update')) {
                            ?>
                            <div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                Data User berhasil diupdate!
                            </div>
                            <?php
                        }
                        ?>
                        <div class="row show-grid">
                                <div class="col-md-12">
                                    <a href="<?= base_url(); ?>master-data/users/input" class="btn btn-primary">Tambah Data</a>
                                </div>
                        </div>
                        <div class="panel panel-default">
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?php
                                if($users != null){
                                    echo $this->load->view('datatables/users');
                                }else{
                                    echo "Data Kosong";
                                }
                            ?>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Script here -->
    <?= $this->load->view('slice/script'); ?>

</body>

</html>
