<div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>ID</th>
                                            <th>username</th>
                                            <th>role</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($users as $row) {
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td>U<?= $row->kd_user; ?></td>
                                            <td><?= $row->username; ?></td>
                                            <td><?= $row->user_lvl; ?></td>
                                            <td><a href="<?= base_url(); ?>master-data/users/edit/<?= $row->kd_user; ?>" class="btn btn-success btn-xs">Edit Password</a>&nbsp
                                            <a href="#deleteUser" data-toggle="modal" class="bank btn btn-danger btn-xs" onClick="modalToogler('<?= $row->kd_user; ?>')">Delete</a></td>
                                         </tr>
                                        <?php
                                            }
                                        ?>  
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="modal fade" id="deleteUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Apakah anda yakin ingin menghapus?</h4>
                                        </div>
                                        <div class="modal-body">
                                            [PERINGATAN]<br>
                                            Data yang sudah dihapus tidak bisa dikembalikan lagi. Pastikan anda benar-benar yakin!
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <a href="" id="delete" class="btn btn-primary">Delete</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                            <!-- Script passing parameter id untuk hapus data -->
                            <script language="javascript">
                                function modalToogler(id_user)
                                {
                                    document.getElementById('delete').href = '<?= base_url() ?>users/hapus/' + id_user;

                                }
                            </script>
                            <!-- /.Script passing parameter id untuk hapus data -->