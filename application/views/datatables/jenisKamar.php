<div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>ID</th>
                                            <th>Jenis Kamar</th>
                                            <th>Harga</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($jenisKamar as $row) {
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td>J<?= $row->kd_jenis_kamar; ?></td>
                                            <td><?= $row->deskripsi; ?></td>
                                            <td><?php echo 'Rp. ' . number_format( $row->harga, 0 , '' , '.' ) . ',-'; ?></td>
                                            <td><a href="<?= base_url(); ?>master-data/jenis-kamar/edit/<?= $row->kd_jenis_kamar; ?>" class="btn btn-success btn-xs">Edit</a>&nbsp
                                            <a href="#deleteBank" data-toggle="modal" class="bank btn btn-danger btn-xs" onClick="modalToogler('<?= $row->kd_jenis_kamar; ?>')">Delete</a></td>
                                         </tr>
                                        <?php
                                            }
                                        ?>  
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            <div class="modal fade" id="deleteBank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Apakah anda yakin ingin menghapus?</h4>
                                        </div>
                                        <div class="modal-body">
                                            [PERINGATAN]<br>
                                            Data yang sudah dihapus tidak bisa dikembalikan lagi. Pastikan anda benar-benar yakin!
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <a href="" id="delete" class="btn btn-primary">Delete</a>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->

                            <!-- Script passing parameter id untuk hapus data -->
                            <script language="javascript">
                                function modalToogler(jenisKamar_id)
                                {
                                    document.getElementById('delete').href = '<?= base_url() ?>jenisKamar/hapus/' + jenisKamar_id;

                                }
                            </script>
                            <!-- /.Script passing parameter id untuk hapus data -->