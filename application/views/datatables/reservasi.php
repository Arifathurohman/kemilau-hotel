<div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>ID</th>
                                            <th>Nama</th>
                                            <th>No HP</th>
                                            <th>Jenis Kamar</th>
                                            <th>No Kamar</th>
                                            <th>Harga</th>
                                            <th>Checked out?</th>
                                            <th>Tgl check in</th>
                                            <th>Tgl check Out</th>
                                            <th>Total Bayar</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($reservasi as $row) {
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td>R<?= $row->kd_rsv; ?></td>
                                            <td><?= $row->nama; ?></td>
                                            <td><?= $row->no_hp; ?></td>
                                            <td><?= $row->jenis_kamar; ?></td>
                                            <td>K<?= $row->no_kamar; ?></td>
                                            <td><?= 'Rp. ' . number_format($row->harga, 0 , '' , '.' ) . ',-'; ?></td>
                                            <td><?= $row->checked_out; ?></td>
                                            <td><?= $row->tgl_in; ?></td>
                                            <td><?= $row->tgl_out; ?></td>
                                            <td><?= 'Rp. ' . number_format($row->total_bayar, 0 , '' , '.' ) . ',-'; ?></td>
                                            <td>
                                            <a href="<?= base_url(); ?>reservasi/check_out/<?= $row->kd_rsv; ?>/<?= $row->kd_kamar; ?>" class="bank btn btn-danger btn-xs">Check Out</a>
                                            </td>
                                         </tr>
                                        <?php
                                            }
                                        ?>  
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->