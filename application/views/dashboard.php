<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $title; ?></title>

    <!--style here -->
    <?= $this->load->view('slice/style'); ?>

</head>

<body>

    <div id="wrapper">

        <!--  navigation bar start here -->
        <?= $this->load->view('slice/navigation'); ?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        <div id="pendapatan" style="height: 300px; width: 100%;"></div>
                    </div>
                    <div class="col-lg-6">
                        <div id="kamar" style="height: 300px; width: 100%;"></div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Script here -->
    <?= $this->load->view('slice/script'); ?>
    <script src="<?= sysadmin_assets ?>bower_components/chart/canvasjs.min.js"></script>
    <script type="text/javascript">

    window.onload = function () {
        var chart = new CanvasJS.Chart("pendapatan", {
            title:{
                text: "Info Transaksi Harian"              
            },
            data: [              
            {
                // Change type to "doughnut", "line", "splineArea", etc.
                type: "column",
                dataPoints: [
                <?php foreach ($totalTransaksiHarian as $row): ?>
                    { label: "<?= $row->tgl_in; ?>",  y: <?= $row->grand_total;?>  },
                <?php endforeach; ?>
                ]
            }
            ]
        });
        chart.render();

        var chart3 = new CanvasJS.Chart("kamar", {
            title:{
                text: "Jumlah Kamar"              
            },
            data: [              
            {
                // Change type to "doughnut", "line", "splineArea", etc.
                type: "doughnut",
                dataPoints: [
                <?php foreach ($totalKamarByJenis as $row): ?>
                    { label: "<?= $row->jenis_kamar; ?>",  y: <?= $row->jumlah;?>  },
                <?php endforeach; ?>
                ]
            }
            ]
        });
        chart3.render();
    }
    </script>
</body>

</html>
