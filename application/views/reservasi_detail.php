<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $title; ?></title>

    <!--style here -->
    <?= $this->load->view('slice/style'); ?>
    <style type="text/css" media="print">
      @page { size: landscape; }
    </style>

</head>

<body>

    <div id="wrapper">

        <!--  navigation bar start here -->
        <?= $this->load->view('slice/navigation'); ?>

        <!-- Page Content -->
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">DETAIL RESERVASI HOTEL</h1>
                        <div class="row show-grid">
                            <?php foreach ($reservasi as $row): ?>
                            <div class="col-md-12">
                                <a href="<?= base_url(); ?>reservasi" class="btn btn-default"><-Back</a>
                                <a href="<?= base_url(); ?>reservasi/cetakNota/<?= $row->kd_rsv; ?>" class="btn btn-success">print Nota</a>
                            </div>
                        </div> 
                        <div class="panel panel-default" id="nota">
                            
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <h1>KEMILAU HOTEL</h1>
                                <div class="col-md-6">
                                    <h3>ID :
                                    <small>R<?= $row->kd_rsv; ?></small>
                                    </h3>
                                    <h3>Nama :
                                    <small><?= $row->nama; ?></small>
                                    </h3>
                                    <h3>No HP :
                                    <small><?= $row->no_hp; ?></small>
                                    </h3>
                                    <h3>Jenis Kamar :
                                    <small><?= $row->jenis_kamar; ?></small>
                                    </h3>
                                     <h3>Harga :
                                    <small><?php echo 'Rp. ' . number_format( $row->harga, 0 , '' , '.' ) . ',-'; ?></small>
                                    </h3>
                                </div>
                                <div class="col-md-6">
                                    <h3>No Kamar :
                                    <small><?= $row->no_kamar; ?></small>
                                    </h3>
                                    <h3>Status Checkout :
                                    <small><?= $row->checked_out; ?></small>
                                    </h3>
                                    <h3>Tanggal Check In:
                                    <small><?= $row->tgl_in; ?></small>
                                    </h3>
                                    <h3>Tanggal Check Out:
                                    <small><?= $row->tgl_out; ?></small>
                                    </h3>
                                     <h3>Total Harga :
                                    <small><?php echo 'Rp. ' . number_format( $row->total_bayar, 0 , '' , '.' ) . ',-'; ?></small>
                                    </h3>
                                </div>
                                <?php endforeach ?>
                                
                            </div>
                        </div>
                        <!-- /.panel -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Script here -->
    <?= $this->load->view('slice/script'); ?>
    <script>
        function printDiv() 
        {

          var divToPrint=document.getElementById('nota');

          var newWin=window.open('','Print-Window');

          newWin.document.open();

          newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

          newWin.document.close();

          setTimeout(function(){newWin.close();},10);

        }
    </script>
</body>
</html>
