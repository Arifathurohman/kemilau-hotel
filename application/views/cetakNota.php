<html>
	<head>
		<title>Cetak Bon</title>
		<style type="text/css">
		/*yang ini buat setting ukuran kertasnya assumsi A4 */
		.A4 {background-color:#FFFFFF;
		left:5px;
		right:5px;
		height:5.0in ; /*Ukuran Panjang Kertas */
		width: 8.50in; /*Ukuran Lebar Kertas */
		page-break-after: always;
		font-family:Georgia, "Times New Roman", Times, serif;
		}
		.A4:last-of-type{
			page-break-after: auto
		}
		img{
			float: left;
			width: 1in;
			height: 1in;
		}
		h1{
			font-size: 19px;
			padding: 0px;
			margin: 0px;
		}
		h2{
			font-size: 16px;
			padding: 0px;
			margin: 2px 0px 1px 0px;
		}
		h3{
			font-size: 14px;
			padding: 0px;
			margin: 2px 0px 0px 0px;
		}
		.cover{
			padding-bottom: 0.1in;

		}
		.table_kiri{
			font-size: 15px;
			width: 40%;
			float: left;
		}

		.table_kanan{
			font-size: 15px;
			width: 60%;
			float: right;
		}
		.table_penjualan{
			font-size: 16px;
			padding-top: 0.1in;
			clear: both;
		}
		tr.dotted td 
		{
		    border-bottom: dotted 1px black;
		    border-top: dotted 1px black;
		}
		/*sampe sini */
		</style>
	</head>
	<body>
                <?php
                $this->load->view('header_nota');
                ?>
               	<?php foreach ($reservasi as $row): ?>
        	 	<tr>
                    <td>R<?= $row->kd_rsv; ?></td>
                    <td><?= $row->nama; ?></td>
                    <td><?= $row->jenis_kamar; ?></td>
                    <td><?= $row->no_kamar; ?></td>

                    <td><?php echo 'Rp. ' . number_format( $row->harga, 0 , '' , '.' ) . ',-'; ?></td>
                    <td><?= $row->tgl_in; ?></td>
                    <td><?= $row->tgl_out; ?></td>
                    <td><?php echo 'Rp. ' . number_format( $row->total_bayar, 0 , '' , '.' ) . ',-'; ?></td>
            	</tr>
            	<?php endforeach; ?>
           		<tr><td></td></tr>
           		<tr><td></td></tr>
            	
       			<tr><td></td></tr>
       			<tr><td></td></tr>
       			<tr>
       				<td></td>
       				<td colspan="3">Tanda Terima</td>
       				<td>Hormat Kami,</td>
       				<td></td>
       	
	</body>
	<script type="text/javascript">
		window.print();
	</script>
</html>