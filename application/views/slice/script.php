<!-- jQuery -->
    <script src="<?= sysadmin_assets ?>bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?= sysadmin_assets ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Bootstrap Datepicker JavaScript -->
    <script src="<?= sysadmin_assets ?>bower_components/datepicker/js/bootstrap-datepicker.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?= sysadmin_assets ?>bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?= sysadmin_assets ?>bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="<?= sysadmin_assets ?>bower_components/datatables-plugins/table-tools/tableTools.js"></script>

    <!-- select bootstrap -->
    <script src="<?= sysadmin_assets ?>bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>

    $(document).ready(function() {
        var t = $('#dataTables-example').DataTable({
            "scrollX": true,
        });

        t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
        } ).draw();

        $('.datepicker').datepicker();

    });
    </script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?= sysadmin_assets ?>bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?= sysadmin_assets ?>dist/js/sidebar.js"></script>