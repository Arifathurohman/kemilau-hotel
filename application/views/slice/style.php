    <!-- Bootstrap Core CSS -->
    <link href="<?= sysadmin_assets ?>bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

     <!-- Bootstrap Datepicker CSS -->
    <link href="<?= sysadmin_assets ?>bower_components/datepicker/css/datepicker.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?= sysadmin_assets ?>bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="<?= sysadmin_assets ?>bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="<?= sysadmin_assets ?>bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
    
    <!-- <link href="<?= sysadmin_assets ?>bower_components/datatables-plugins/table-tools/tableTools.css" rel="stylesheet"> -->
    
    <!-- Custom CSS -->
    <link href="<?= sysadmin_assets ?>dist/css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= sysadmin_assets ?>bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Bootstrap Selects -->
    <link href="<?= sysadmin_assets ?>bower_components/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" >


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->