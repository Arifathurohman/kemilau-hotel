
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="">KEMILAU HOTEL SYSTEM</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                 <!-- /.dropdown -->
                 <?php
                    $this->db->select('kd_rsv, nama, tgl_out');
                    $this->db->where('tgl_out', date('Y-m-d'));
                    $this->db->from('reservasi');

                    $hasil = $this->db->get()->result();
                 ?>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                    <?php foreach ($hasil as $row) :?>
                        <li>
                            <a href="<?= base_url(); ?>reservasi">
                                <div>
                                    Checkout : Kd rsv <?= $row->kd_rsv ?> <br> atas nama <?= $row->nama; ?>
                                    <span class="pull-right text-muted small"><?= $row->tgl_out; ?></span>
                                </div>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?= base_url(); ?>auth/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                    <?php if ($this->session->userdata('user_lvl') == 'owner'): ?>
                        <li>
                            <a href="<?= base_url(); ?>dashboard"><i class="fa fa-table fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-sitemap fa-fw"></i> Master Data<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?= base_url(); ?>master-data/jenis-kamar">Jenis Kamar</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url(); ?>master-data/kamar">Kamar</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url(); ?>master-data/users">Users</a>
                                    </li>
                                </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    <?php endif ?>
                        <li>
                            <a href="<?= base_url(); ?>reservasi"><i class="fa fa-usd fa-fw"></i> Reservasi</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>