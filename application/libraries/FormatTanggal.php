<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class FormatTanggal{

  public function bulanSekarang(){
    $array_bulan = array(
      1 => 'Januari','Februari','Maret', 'April', 'Mei', 'Juni','Juli','Agustus', 'September','Oktober', 'November','Desember'
      );

    $formatBulan = $array_bulan[date('n')];

    return $formatBulan;
  }

  public function tahunBulanHari($cal_date){
  	if($cal_date == null){
  		$date = $cal_date;
  	}else{
  		$date=date('Y-m-d',strtotime($cal_date));
  	return $date;
  	
  	}
  }

  public function hariBulanTahun($cal_date){
    $date=date('d/m/Y',strtotime($cal_date));
    return $date;
  }

}

