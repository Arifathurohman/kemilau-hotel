<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['default_controller'] = "login";
$route['404_override'] = '';

/* Routing Master Data */
$route['master-data/jenis-kamar'] = "jenisKamar";
$route['master-data/jenis-kamar/input'] = "jenisKamar/input";
$route['master-data/jenis-kamar/edit/(:num)'] = "jenisKamar/edit/$1";

$route['master-data/kamar'] = "kamar";
$route['master-data/kamar/input'] = "kamar/input";
$route['master-data/kamar/edit/(:num)'] = "kamar/edit/$1";

$route['master-data/users'] = "users";
$route['master-data/users/input'] = "users/input";
$route['master-data/users/edit/(:num)'] = "users/edit/$1";


$route['master-data/users'] = "users";
$route['master-data/users/input'] = "users/input";
$route['master-data/users/edit/(:num)'] = "users/edit/$1";

/* Routing Dashboard */
$route['dashboard'] = "dashboard";

/* Routing Reservasi */
$route['reservasi'] = "reservasi";
$route['reservasi/check-in'] = "reservasi/check_in";
$route['reservasi/check-out/detail/(:num)'] = "reservasi/detail/$1";






/* End of file routes.php */
/* Location: ./application/config/routes.php */